import { Box, Container, Typography } from "@mui/material";
import React from "react";
import { Route, Routes } from "react-router-dom";
import Animals from "./pages/Animals";
import Login from "./pages/Login";
import Register from "./pages/Register";

const App = () => {
  return (
    <Container>
      <Box mt={3} display="flex" flexDirection="column" alignItems="center">
        <Typography variant="h3">JWT Auth</Typography>
        <Routes>
          <Route path="/" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/animals" element={<Animals />} />
        </Routes>
      </Box>
    </Container>
  );
};

export default App;
