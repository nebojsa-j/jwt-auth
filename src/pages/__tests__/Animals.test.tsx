import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Animals from "../Animals";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

const renderComponent = () => {
  const history = createMemoryHistory();
  return {
    ...render(
      <Router location={history.location} navigator={history}>
        <Animals />
      </Router>
    ),
  };
};

describe("Animals component", () => {
  it("renders elements properly", () => {
    renderComponent();
    expect(screen.getAllByRole("button").length).toBe(11);
    expect(screen.getByRole("heading")).toBeInTheDocument();
    expect(screen.getByRole("grid")).toBeInTheDocument();
    expect(screen.getByRole("progressbar")).toBeInTheDocument();
    expect(screen.getByRole("row")).toBeInTheDocument();
    expect(screen.getByRole("navigation")).toBeInTheDocument();
    expect(screen.getByRole("list")).toBeInTheDocument();
  });
});
