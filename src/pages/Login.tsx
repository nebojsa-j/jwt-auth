import React, { useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { login } from "../services/authService";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  TextField,
  Typography,
  LinearProgress,
} from "@mui/material";

const Login = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const initialValues = {
    email: "",
    password: "",
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("You must enter valid email.")
      .required("Required."),
    password: Yup.string()
      .required("Required.")
      .min(5, "Password must have min 5 characters")
      .max(50, "Password must have max 50 characters")
      .matches(
        /^(?=.*[a-zA-Z])(?=.*[0-9])/,
        "Password must contain at least one letter and one number"
      ),
  });

  const handleLogin = (values: typeof initialValues) => {
    const { email, password } = values;
    setLoading(true);
    login(email, password)
      .then((response) => {
        setLoading(false);
        navigate("/animals");
      })
      .catch((error) => {
        alert(error.response.data.message);
        setLoading(false);
      });
  };

  return (
    <Box mt={5} maxWidth="sm">
      <Typography variant="h4" textAlign="center">
        Log In
      </Typography>
      <Box mt={3}>
        {loading && <LinearProgress />}
        {!loading && (
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleLogin}
          >
            {({ values, errors, touched, handleChange, handleSubmit }) => (
              <form noValidate onSubmit={handleSubmit}>
                <TextField
                  name="email"
                  type="email"
                  label="Email"
                  fullWidth
                  margin="normal"
                  value={values.email}
                  onChange={handleChange}
                  error={touched.email && !!errors.email}
                  helperText={touched.email && errors.email}
                />
                <TextField
                  name="password"
                  type="password"
                  label="Password"
                  fullWidth
                  margin="normal"
                  value={values.password}
                  onChange={handleChange}
                  error={touched.password && !!errors.password}
                  helperText={touched.password && errors.password}
                />
                <Box mt={3}>
                  <Button type="submit" variant="contained" fullWidth>
                    Submit
                  </Button>
                </Box>
              </form>
            )}
          </Formik>
        )}
      </Box>
    </Box>
  );
};

export default Login;
