import React, { useEffect, useState } from "react";
import { getAnimals } from "../services/userService";
import { Box, Pagination, Typography, Button } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { v4 as uuidv4 } from "uuid";
import { logout } from "../services/authService";
import { useNavigate } from "react-router-dom";

const columns = [
  {
    field: "averageLifeExpectancy",
    headerName: "Average Life Expectancy",
    width: 175,
  },
  { field: "origin", headerName: "Origin", width: 120 },
  { field: "gender", headerName: "Gender" },
  { field: "type", headerName: "Type" },
  { field: "energyLevel", headerName: "Energy Level" },
  { field: "breed", headerName: "Breed", width: 150 },
  { field: "flying", headerName: "Flying" },
  { field: "wingSpan", headerName: "Wing Span" },
];

const Animals = () => {
  const [loading, setLoading] = useState(true);
  const [tableData, setTableData] = useState([]);
  const [page, setPage] = useState(1);
  const navigate = useNavigate();

  useEffect(() => {
    getAnimals(page).then(
      (response) => {
        setTableData(response.data.items);
        setLoading(false);
      },
      (error) => {
        alert(error.response.data.message);
        setLoading(false);
      }
    );
  }, [page]);

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  const handleClick = () => {
    logout();
    navigate("/login");
  };

  return (
    <Box mt={3} display="flex" flexDirection="column" alignItems="center">
      <Box>
        <Typography variant="h4">Animals</Typography>
      </Box>
      <Box mt={4} width={960}>
        <DataGrid
          rows={tableData}
          getRowId={() => uuidv4()}
          columns={columns}
          autoHeight
          pageSize={20}
          loading={loading}
          rowsPerPageOptions={[20]}
        />
      </Box>
      <Box my={3}>
        <Pagination count={6} page={page} onChange={handleChange} />
      </Box>
      <Box my={2}>
        <Button variant="contained" onClick={handleClick}>
          Log Out
        </Button>
      </Box>
    </Box>
  );
};

export default Animals;
