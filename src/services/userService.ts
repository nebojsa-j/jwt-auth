import axios, { AxiosRequestHeaders } from "axios";
import { authHeader } from "./authService";
import { API_URL } from "./authService";

export const getAnimals = (query: number) => {
  return axios.get(`${API_URL}animals?page=${query}`, {
    headers: authHeader() as AxiosRequestHeaders,
  });
};
