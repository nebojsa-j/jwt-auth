import axios from "axios";
export const API_URL = "https://assignment-api.alterset.space/";

export const register = (username: string, email: string, password: string) => {
  return axios.post(API_URL + "auth/register", {
    username,
    email,
    password,
  });
};

export const login = async (email: string, password: string) => {
  const response = await axios.post(API_URL + "auth/login", {
    email,
    password,
  });
  if (response.data.token) {
    localStorage.setItem("token", response.data.token);
  }
  return response.data;
};

export const logout = () => {
  localStorage.removeItem("token");
};

export const authHeader = () => {
  const token = localStorage.getItem("token");
  if (token) {
    return { Authorization: "Bearer " + token };
  } else {
    return {};
  }
};
